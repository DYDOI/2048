window.onload = function() {
  let ready = document.getElementById('confirm'); //кнопка готовности для создания сетки @return размер игрового поля
  let size = document.getElementById('size'); //возвращаемое значение размера поля
  let grid = document.getElementById('grid'); //игровое поле
  let row, cells, cellNumber = 0,
    order = 0;
  let matrix;
  let emptyCells;

  // генерация сетки игрового поля
  ready.addEventListener('click', () => {
    // matrix = [[2, 4, 8, 16],[32, 64, 128, 256],[512, 1024, 2048, 0],[0, 0, 0, 0]];
    matrix = [];
    while (grid.firstChild) grid.removeChild(grid.firstChild);
    cellNumber = 0;
    for (let g = 0; g < size.value; g++) {
      matrix[g] = new Array();
      for (let f = 0; f < size.value; f++) {
        matrix[g][f] = 0;
      }
    }

    for (let i = 0; i < size.value; i++) {
      row = document.createElement('div');
      row.id = "row" + i;
      row.style.display = "flex";
      row.style.width = "max-content";

      for (let j = 0; j < size.value; j++) {
        cells = document.createElement('div');
        cells.id = "cell" + cellNumber;
        // cells.style.padding = "calc(2em - 0.25em)";
        cells.style.border = "1px solid black";
        cells.style.display = "flex";
        cells.style.justifyContent = "center";
        cells.style.alignItems = "center";
        cells.style.height = "4em";
        cells.style.width = "4em";
        cellNumber += 1;
        row.appendChild(cells);
      }

      grid.appendChild(row);
    }

    start();
  });

  // запуск всей игры
  const start = () => {
    document.onkeydown = function(e) {
      switch (e.keyCode) {
        case 38:
        playerTurn(1, 0, 0, 0);
        nextTurn();
        break;
        case 40:
        playerTurn(0, 1, 0, 0);
        nextTurn();
        break;
        case 39:
        playerTurn(0, 0, 0, 1);
        nextTurn();
        break;
        case 37:
        playerTurn(0, 0, 1, 0);
        nextTurn();
        break;
        default:
          return;
          break;

      }
    }

    console.log(matrix);
    updateData();
    nextTurn();
  };

  // Обновление данных в сетке html
  const updateData = () => {
    emptyCells = size.value * size.value;
    let count = 0;
    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix.length; j++) {
        document.getElementById(`cell${count}`).innerHTML = matrix[i][j];
        document.getElementById(`cell${count}`).style.backgroundColor = selectColor(matrix[i][j]);;
        if (matrix[i][j] !== 0) emptyCells -= 1;
        count += 1;
      }
    }
    console.log(`total empty cells: ${emptyCells}`);
    console.log(matrix);
  };


  const selectColor = (number) => {
    switch (number) {
      case 0:
        return '#ffffff';
        break;
      case 2:
        return '#d9d9d9';
        break;
      case 4:
        return '#ffffe6';
        break;
      case 8:
        return '#ffcc80';
        break;
      case 16:
        return '#ffb84d';
        break;
      case 32:
        return '#ff9900';
        break;
      case 64:
        return '#ff3300';
        break;
      case 128:
        return '#ffff66';
        break;
      case 256:
        return '#f5ff66';
        break;
      case 512:
        return '#e8ff66';
        break;
      case 1024:
        return '#d9ff66';
        break;
      case 2048:
        return '#ccff66';
        break;
      default:
        return;
        break;

    }
  }

  // Переход на следующии ход (спавн 2/4 в свободной клетке)
  const nextTurn = () => {
    let newValue = Math.floor(Math.random() * 2) ? 2 : 4;
    let row = Math.floor(Math.random() * size.value),
      collumn = Math.floor(Math.random() * size.value);
    if (emptyCells > 0) {
      console.log("enter");
      while (matrix[row][collumn] !== 0) {
        row = Math.floor(Math.random() * size.value);
        collumn = Math.floor(Math.random() * size.value);
      }
      matrix[row][collumn] = newValue;
    }
    updateData();
  };


  const sort = (row, up, down, left, right) => {
    let flag = 0;
    if (right) {
      row.forEach((value, i) => {
        if (row[i + 1] === 0 && row[i] !== 0) {
          row[i + 1] = value;
          row[i] = 0;
          flag = 1;
        }

        if (row[i] === row[i + 1]) {
          row[i + 1] += row[i];
          row[i] = 0;
        }
        if (flag) sort(row, 0, 0, 0, 1);
      });
    }

    if (left) {
      row.forEach((value, i) => {
        if (row[i] === 0 && (row[i + 1] !== 0 && row[i + 1] !== undefined)) {
          row[i] = row[i + 1];
          row[i + 1] = 0;
          flag = 1;
        }

        if (row[i] === row[i + 1]) {
          row[i] += row[i + 1];
          row[i + 1] = 0;
        }
        if (flag) sort(row, 0, 0, 1);
      });
    }
  }

  const playerTurn = (up, down, left, right) => {
    let flag = 0;

    if (right) {
      matrix.forEach(row => {
        sort(row, 0, 0, 0, right);
      });
    }

    if (left) {
      matrix.forEach(row => {
        sort(row, 0, 0, left);
      });
    }

    if (down) {
      for (let i = 0; i < matrix.length - 1; i++) {
        for (let j = 0; j < matrix.length; j++) {
          if (matrix[i][j] !== 0 && (matrix[i + 1][j] === 0)) {
            matrix[i + 1][j] = matrix[i][j];
            matrix[i][j] = 0;
          }

          if (matrix[i][j] === matrix[i + 1][j]) {
            matrix[i + 1][j] += matrix[i][j];
            matrix[i][j] = 0;
          }

        }
      }
    }

    if (up) {
      for (let i = matrix.length - 1; i !== 0; i -= 1) {
        for (let j = 0; j < matrix.length; j++) {
          console.log(matrix[i][j]);
          if (matrix[i][j] !== 0 && (matrix[i - 1][j] === 0)) {
            matrix[i - 1][j] = matrix[i][j];
            matrix[i][j] = 0;
          }

          if (matrix[i][j] === matrix[i - 1][j]) {
            matrix[i - 1][j] += matrix[i][j];
            matrix[i][j] = 0;
          }

        }
      }
    }


    updateData();
  }
}
